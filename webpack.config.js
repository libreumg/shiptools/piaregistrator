const webpack = require('webpack');
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const path = require('path');
const serverConfig = {
    target: 'node',
    mode: 'production',
    entry: './src/server.js', 
    output: {
        path: path.resolve(__dirname, 'debian', 'piaemailcollector', 'opt', 'piaemailcollector', 'bin'),
        filename: 'email-collector.js',
    },  
    plugins: [
      new webpack.IgnorePlugin({ resourceRegExp: /^pg-native$/ })
    ],
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin()]
    }
}

const clientConfig = {
  target: 'web',
  mode: 'production',
  entry: './public/js/main.js', 
  output: {
      path: path.resolve(__dirname, 'debian', 'piaemailcollector', 'opt', 'piaemailcollector', 'public', 'js'),
      filename: 'main.js',
  },  
  module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader'
        }
      ]
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          { from: "./public/images", to: "../images" },
          { from: "./public/css/*.css", to: "../../" },
          { from: "./public/views", to: "../views" }
        ],
      }),
    ],
    optimization: {
      minimize: true,
      minimizer: [new TerserPlugin()]
    }
}

module.exports = [serverConfig, clientConfig]