# discontinued

This application has become **deprecated** because of https://gitlab.com/libreumg/pia/emailcollectorapp

# email-collector

collects emails from participants and add them to a pia instance as well as to a postgres database

# setup

## database

The email-collector works with a postgreSQL database that keeps the information about valid pseudo-ids and registration events. It may contain e-mail addresses also.

To setup the database, proceed with the following steps:

### install a postgreSQL database, at least version 10

- create a database `email_collector`
- create a user `interface_email_collector` for that database
- grant the needed privileges for the new user

```bash
postgres@debian# createdb email_collector
postgres@debian# createuser interface_email_collector --interactive -W
...
postgres@debian# psql email_collector
postgres@piaregistrator# grant connect on database email_collector to interface_email_collector;
```

### run the setup script to create the tables

- run the scripts from the `setup` folder of this repository

```bash
postgres@debian# psql email_collector -f email-collector.sql
```

## Credentials

The email-collector needs access to your postgres and pia credentials. Therefore you have to store them as enviroment variables or in a _.env_ file in the root directory. The following variables are required:

```
SERVER_PORT=3000
SESSION_SECRET=session_password

DB_HOST=localhost
DB_PORT=5432
DB_NAME=email_collector
DB_USER=yourUsername
DB_PASSWORD=yourPassword

PIA_PM_USERNAME=USERNAME
PIA_PM_PASSWORD=USERPASSWORD
PIA_BASE_URL=https://pia-develop.netzlink.com
```

## Dependencies

The next step is to install all the required depencies. This can be done with

```
npm install
```

Or you install them one by one:

```bash
npm install ejs express express-session express-validator memorystore pg-promise cross-fetch
```

```bash
npm install babel-loader dotenv @babel/cli @babel/core @babel/node @babel/preset-env webpack webpack-cli nodemon terser-webpack-plugin copy-webpack-plugin -D
```

# start the application

## for development purpose

```bash
npm run devStart
```

## for productive usage

### on demand, call the system ssl libraries before running the build
```bash
export NODE_OPTIONS=--openssl-legacy-provider
```

```bash
npm run build
```

Now you can build it as a debian package:

```bash
debian/build.sh
```

# Usage

It is assumed that each proaband has a unique pseudo ID. This ID is passed to the web server via a URL with the path `/PseudoID`. Alternatively, the pseudo ID can be passed manually without any path or with `/id`.
If the participant enters valid credentials, the email will be submitted to the pia instance and if the participant agrees, the email will also be added to the postgres database.
