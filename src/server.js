// modules
const ejs = require("ejs").__express;
const express = require('express')
const {check, validationResult} = require("express-validator")
const expressSession = require("express-session")
const MemoryStore = require('memorystore')(expressSession) // default memoryStore for express-session will lead to memory leaks, see https://github.com/roccomuso/memorystore
const pia = require('./piaInterface')
const pgp = require('pg-promise')()
require('dotenv').config()

// database configuration
const cn = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    max: 100 // use up to 100 connections
};
const db = pgp(cn)

// web server setup
const app = express()
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.set('view engine', 'ejs')
app.set('views', __dirname + '/../public/views')
app.engine('ejs', ejs)
// app.set("trust proxy", 1)
app.use(expressSession({
    store: new MemoryStore({
        // prune expired entries every 30 minutes
        checkPeriod: (60*1000) * 30                     
    }),
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false, 
    resave: false,
    cookie: { 
        sameSite: 'strict',
        httpOnly: true,
        //secure: true
    } 
}))

// web server routes
app.route('/')
.get( (req, res) => {
    res.render("index", {
        innerForm : 'inputForm'
    })
})
.post(
    [ 
        // check email syntax
        check('email', "Ungültige E-Mail").if(check('id').custom((value, {req})=>{
            if (req.body.saveEmailForPia === 'yes' || req.body.saveEmailForShip === 'yes')
                return true
        })).isEmail(),

        // validate pseudo-ID and password with pia.login()
        check('id').custom(async(value, {req}) => { 
            await pia.login(
                process.env.PIA_BASE_URL,
                req.body.id,
                req.body.password
            )
            .catch(err => {
                console.log(new Date(), "Failed to login with user credentials for pseudo-ID %s", req.body.id)
                throw new Error('Login fehlgeschlagen. Bitte überprüfen Sie Ihre Pseudo-ID und Ihr Passwort.');
            })
        })
    ], 
    async (req, res) => {

        // if any errors occured -> redirect to inputForm and show error messages
        const errors = validationResult(req)
        if (!errors.isEmpty()){
            console.log(new Date(), "Redirecting to inputForm due to validation errors: ", errors.array())
            req.session.errors = errors.array()
            res.render("index", {
                innerForm : 'inputForm', 
                pseudoId: req.body.id,
                email: req.body.email,
                password: req.body.password,
                saveEmailForShip: req.body.saveEmailForShip,
                saveEmailForPia: req.body.saveEmailForPia
            })
            return
        }

        // default message after submitting
        let msg  = 'Vielen Dank für Ihre Zeit.'

        // get pmToken
        let pmToken = await pia.login(
            process.env.PIA_BASE_URL,
            process.env.PIA_PM_USERNAME,
            process.env.PIA_PM_PASSWORD
        ).catch(err => {
            console.log(new Date(), "Failed to get pmToken -  ", err)
            msg = 'Die E-Mail-Adresse konnte leider nicht an PIA übergeben werden. Bitte versuchen Sie es später erneut.'
        })
    
        // add email to pia
        await pia.addEmail(
            process.env.PIA_BASE_URL, 
            pmToken,
            req.body.id,
            (req.body.saveEmailForPia === 'yes') ? req.body.email : ''
        ).catch(err => {
            console.log(new Date(), "Failed to add email to pia app - ", err)
            msg = 'Die E-Mail-Adresse konnte leider nicht an PIA übergeben werden. Bitte versuchen Sie es später erneut.'
        })

        // default sql values for postgres db query
        let sql_consentPia = (req.body.saveEmailForPia === 'yes') ? 'now()' : 'null'
        let sql_consentShip = (req.body.saveEmailForShip === 'yes') ? 'now()' : 'null'
        let sql_email = (req.body.saveEmailForShip === 'yes') ? req.body.email : '' 
        
        // sql query template
        let sqlQuery = `INSERT INTO t_person(pseudo_id, email, registered, consent_pia, consent_ship)
                        values ('${req.body.id}', '${sql_email}', true, ${sql_consentPia}, ${sql_consentShip})
                        on conflict (pseudo_id) do update
                            set email = excluded.email,
                            registered = excluded.registered,
                            consent_pia = excluded.consent_pia,
                            consent_ship = excluded.consent_ship;`

        // update postgres db
        await db.any(sqlQuery).catch((err) => {
            console.log(new Date(), "Failed to insert email into postgres database - ", err)
            msg = 'Die E-Mail Adresse konnte leider nicht an das Probandenmanagement übergeben werden. Bitte versuchen Sie es später erneut.'
        })

        // render status msg
        res.render("index", {
            innerForm : 'msgForm', 
            msg
        })

        // delete session variables
        req.session.destroy()
    }
)
app.route('/id')
.get( (req, res) => {
    res.render("index", {
        innerForm : 'inputForm'
    })
})
app.route('/id/:pseudoId')
.get((req, res) => {
    res.render("index", {
        innerForm : 'inputForm',
        pseudoId: req.params.pseudoId
    })
})

app.route('/alerts')
.post((req, res) => {
    res.send({errors: req.session.errors}).status(200)
    req.session.destroy()
})

app.use(express.static(__dirname+'/../public'));
app.listen(process.env.SERVER_PORT, () => console.log(new Date(), "Web server is listening on port: ", process.env.SERVER_PORT))
